#artist: a person who makes the songs and will go in a library and albums

class Artist:

    def __init__(self, name, genre):
        self.name = name
        self.genre = genre

    def to_string(self):
        s = (self.genre + ' artist ' + self.name)
        return s

    def get_name(self):
        return self.name

    def get_genre(self):
        return self.genre

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)

