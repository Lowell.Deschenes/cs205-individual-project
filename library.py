#library that holds artists, songs and albums

class Library:

    def __init__(self):
        self.songs = set()
        self.albums = set()
        self.artists = set()

    def add_album(self, album):
        #check to see if album is already in library and add if not add
        if album not in self.albums:
            self.albums.add(album)
        else:
            print ("album is already in library")

        #check to see if artist is in library and add if not add
        if album.get_artist() not in self.artists:
            self.artists.add(album.get_artist())

        #check to see if the songs are in library and if not add
        for s in album.get_songs():
            if s not in self.songs:
                self.songs.add(s)

    def add_song(self, song):
        if song in self.songs:
            print("song is already in library")
        else:
            self.songs.add(song)

        if song.get_artist() not in self.artists:
            self.artists.add(song.get_artist())

    def get_artists(self):
        return self.artists

    def get_albums(self):
        return self.albums

    def get_songs(self):
        return self.songs

    def artist_is_in_library(self, artist):
        if artist in self.artists:
            return True
        else:
            return False

    def album_is_in_library(self, album):
        if album in self.albums:
            return True
        else:
            return False

    def song_is_in_library(self, song):
        if song in self.songs:
            return True
        else:
            return False




