# album: a container for an artists and songs

class Album:

    def __init__(self, title, artist):
        self.title = title
        self.artist = artist
        self.songs = set()

    def to_string(self):
        s = (self.title + ' by ' + self.artist.get_name() + ' with ' + str(len(self.songs)) + ' song(s)')
        return s

    def get_title(self):
        return self.title

    def get_artist(self):
        return self.artist

    def __eq__(self, other):
        return (self.title == other.title and self.artist == other.artist)

    def __hash__(self):
        return hash((self.title, self.artist))

    def add_songs(self, songs):
        for s in songs:
            if s not in self.songs and s.get_artist() == self.artist:
                self.songs.add(s)

    def add_song(self, song):
        if song not in self.songs and song.get_artist() == self.artist:
            self.songs.add(song)
            return True
        else:
            print('song already in album or artist does not match')
            return False

    def add_song_incorrect_implementation(self, song):
        if song not in self.songs:
            self.songs.add(song)
            return True
        else:
            return False

    def get_songs(self):
        return self.songs

    def is_in_album(self, song):
        if song in self.songs:
            print(song.to_string() + " is in album " + self.to_string())
            return True
        else:
            print('checked for song ' + song.to_string() + ' in album ' + self.to_string() + ': failed')
            return False

