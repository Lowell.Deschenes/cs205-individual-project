import unittest
import library
import album
import song
import artist

# unit tests for my song Library application

class TestCheckout(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # called one time, at the very beginning
        print('setUpClass()')

        # create a few songs and artists
        title_1 = 'Candle'
        title_2 = 'A Figure 8'
        title_3 = 'Pocketknife'
        artist_a = 'Buck Meek'
        artist_b = 'Cowgirl Clue'
        genre_a = "folk"
        genre_b = 'electronic'
        # we'll use the songs and the artists in the tests, so make them class variables
        cls.buck = artist.Artist(artist_a, genre_a)
        cls.cowgirl = artist.Artist(artist_b, genre_b)
        cls.song_1 = song.Song(title_1, cls.buck)
        cls.song_2 = song.Song(title_2, cls.cowgirl)
        cls.song_3 = song.Song(title_3, cls.buck)

    @classmethod
    def tearDownClass(cls):
        # called one time, at the very end--if you need to do any final cleanup, do it here
        print('tearDownClass()')

    def setUp(self):
        # called before every test
        print('setUp()')
        self.library = library.Library()
        self.two_saviors = album.Album('two saviors', self.buck)
        self.one_savior = album.Album('one savior', self.buck)

    def tearDown(self):
        # called after every test
        print('tearDown()')
# -------------------------------------------------------------

    def test_add_song_to_album_incorrect(self):
        # this tests the incorrect version of add_song to album: this test will fail

        # try to add buck meek song to buck meek album - should return true
        rc = self.two_saviors.add_song_incorrect_implementation(self.song_1)
        self.assertTrue(rc)

        # try to add cowgirl clue song to buck meek album - should return false
        rc = self.two_saviors.add_song_incorrect_implementation(self.song_2)
        self.assertFalse(rc)


        # try to add the same song - should return false
        rc = self.two_saviors.add_song_incorrect_implementation(self.song_1)
        self.assertFalse(rc)

        #check that the album has only one song
        songs = self.two_saviors.get_songs()
        self.assertEqual(len(songs), 1)


#-------------------------------------------------------------

    def test_add_song_to_album_correct(self):
        print('test return correct')
        # this tests the correct implementation of add_song to album: it will succeed

        # try to add buck meek song to buck meek album - should return true
        rc = self.two_saviors.add_song(self.song_1)
        self.assertTrue(rc)

        # try to add cowgirl clue song to buck meek album - should return false
        rc = self.two_saviors.add_song(self.song_2)
        self.assertFalse(rc)

        # try to add the same song - should return false
        rc = self.two_saviors.add_song(self.song_1)
        self.assertFalse(rc)

        # check that the album has only one song
        songs = self.two_saviors.get_songs()
        self.assertEqual(len(songs), 1)

#-------------------------------------------------------------

    def test_add_song_to_library_1(self):
        # check that the library shows that no artists are in library
        artists = self.library.get_artists()
        self.assertEqual(len(artists), 0)

        # add a buck meek song to library
        self.library.add_song(self.song_1)

        #add another song by same artist
        self.library.add_song(self.song_3)

        # check that the number of artists in library is 1
        artists = self.library.get_artists()
        self.assertEqual(len(artists), 1)

        # check that the library shows buck meek is in library
        rc = self.library.artist_is_in_library(self.buck)
        self.assertTrue(rc)

#-------------------------------------------------------------

    def test_add_song_to_library_2(self):
        # check that the library shows that no songs are in library
        songs = self.library.get_songs()
        self.assertEqual(len(songs), 0)

        # add a cowgirl clue song
        self.library.add_song(self.song_2)

        # try to add the same song
        self.library.add_song(self.song_2)

        # check that the the library has 1 song
        songs = self.library.get_songs()
        self.assertEqual(len(songs), 1)

        # check that the library shows figure 8 is in library
        rc = self.library.song_is_in_library(self.song_2)
        self.assertTrue(rc)


#-------------------------------------------------------------

    def test_add_album_to_library_1(self):
        # check that the library shows that no artists are in library
        artists = self.library.get_artists()
        self.assertEqual(len(artists), 0)

        #make a buck meek album
        self.two_saviors.add_song(self.song_1)

        #make another buck meek album
        self.one_savior.add_song(self.song_3
                                 )
        ## add album to library
        self.library.add_album(self.two_saviors)

        #add the other album to library
        self.library.add_album(self.one_savior)

        # check that the number of artists in library is 1
        artists = self.library.get_artists()
        self.assertEqual(len(artists), 1)


        # check that the library shows buck meek is in library
        rc = self.library.artist_is_in_library(self.buck)
        self.assertTrue(rc)

# -------------------------------------------------------------

    def test_add_album_to_library_2(self):
        # check that the library shows that no artists are in library
        albums = self.library.get_albums()
        self.assertEqual(len(albums), 0)

        # add songs to buck meek album
        songs = [self.song_1, self.song_3]
        self.two_saviors.add_songs(songs)

        ## add album to library
        self.library.add_album(self.two_saviors)

        ## try to add the same album to library
        self.library.add_album(self.two_saviors)

        # check that the number of artists in library is 1
        albums = self.library.get_artists()
        self.assertEqual(len(albums), 1)

        # check that the library shows two saviors is in library
        rc = self.library.album_is_in_library(self.two_saviors)
        self.assertTrue(rc)



#=================================

if __name__ == "__main__":
    unittest.main()
