#song: a piece of work that will go in a library and album

class Song:

    def __init__(self, title, artist):
        self.title = title
        self.artist = artist

    def to_string(self):
        s = ('"' + self.title + '" by ' + self.artist.get_name())
        return s

    def get_title(self):
        return self.title

    def get_artist(self):
        return self.artist

    def __eq__(self, other):
        return self.title == other.title and self.artist == other.artist

    def __hash__(self):
        return hash((self.title, self.artist))
