import library
import artist
import song
import album


def simple_runtime_test(thelib):

    title_1 = 'Song One Title'
    title_2 = 'Song Two Title'
    title_3 = 'Song Three Title'
    title_4 = 'Song Four Title'
    title_5 = 'Song Five Title'
    name_1 = "Artist 1"
    name_2 = "Artist 2"
    name_3 = "Artist 3"
    genre_1 = "Genre 1"
    album_1 = "Album 1"
    album_2 = "Album 2"

    artist_1 = artist.Artist(name_1, genre_1)
    artist_2 = artist.Artist(name_2, genre_1)
    artist_3 = artist.Artist(name_3, genre_1)

    #lets see if we can get the genre
    print("genre should be 'Genre 1' got: " + artist_1.get_genre() + "\n")

    song_1 = song.Song(title_1, artist_1)
    song_2 = song.Song(title_2, artist_2)
    song_3 = song.Song(title_3, artist_1)
    song_4 = song.Song(title_4, artist_2)
    song_5 = song.Song(title_5, artist_3)

    # add two of the songs to a list
    songs = [song_1, song_3]

    album_1 = album.Album(album_1, artist_1)
    album_2 = album.Album(album_2, artist_2)

    #add songs to album 1
    album_1.add_songs(songs)

    #add songs to album 2
    album_2.add_song(song_2)

    print("songs from album 1: ")
    alb1_songs = album_1.get_songs()
    for s in alb1_songs:
        print(s.to_string() + '\n')

    print("songs from album 2: ")
    alb2_songs = album_2.get_songs()
    for s in alb2_songs:
        print(s.to_string() + '\n')

    #add albums
    thelib.add_album(album_1)
    thelib.add_album(album_2)

    #add a singular song
    thelib.add_song(song_4)
    thelib.add_song(song_5)


    #get all the songs artists and albums in the library
    lib_songs = thelib.get_songs()
    lib_artists = thelib.get_artists()
    lib_albums = thelib.get_albums()


    print("songs from library: ")
    for s in lib_songs:
        print(s.to_string() + '\n')

    print("artists from library: ")
    for art in lib_artists:
        print(art.to_string() + '\n')

    print("albums from library: ")
    for alb in lib_albums:
        print(alb.to_string() + '\n')


    #lets see if we can add the same song
    thelib.add_song(song_5)

    #lets see if it added the an artist or song
    lib_songs = thelib.get_songs()
    lib_artists = thelib.get_artists()
    print("should get 5 and 3 got: ")
    print(len(lib_songs))
    print(len(lib_artists))
    print("\n")


    #lets see if we can add the same album
    thelib.add_album(album_1)

    #lets see if it added the an artist or album
    lib_albums = thelib.get_albums()
    lib_artists = thelib.get_artists()
    print("should get 2 and 3 got:")
    print(len(lib_albums))
    print(len(lib_artists))
    print("\n")

    #lets try to add the same song to an album
    album_2.add_song(song_2)

    #lets see if it added the song
    alb2_songs = album_2.get_songs()
    print(len(alb2_songs))
    print("\n")

    #lets check the is in functions of library
    p = thelib.artist_is_in_library(artist_3)
    if p:
        print(artist_3.to_string() + ' is in library')
        print("\n")

    # lets check the is in functions of library
    p = thelib.album_is_in_library(album_1)
    if p:
        print(album_1.to_string() + ' is in library')
        print("\n")

    # lets check the is in functions of library
    p = thelib.song_is_in_library(song_3)
    if p:
        print(song_3.to_string() + ' is in library')
        print("\n")
        
    # is_in of the album
    album_1.is_in_album(song_5)
    print("\n")

    album_1.is_in_album(song_1)

#----------------------------------------

def main():
  mylib = library.Library()

  simple_runtime_test(mylib)

#----------------------------------------

main()
